package com.sort;

import java.util.Arrays;
import java.util.Scanner;

/**
 * The App class provides a simple command-line application that reads
 * up to ten integers from the user, sorts them in ascending order, and
 * prints the sorted integers to the standard output.
 */
public class App {

    /**
     * The main method serves as the entry point of the application.
     *
     * @param args the command-line arguments (not used)
     */
    public static void main(String... args) {
        System.out.println("Enter 10 integers: ");
        String userInput = readUserInput();
        String[] numbersStr = validateUserInput(userInput);
        int[] numbers = convertToInteger(numbersStr);
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }

    /**
     * Reads user input from the standard input.
     *
     * @return a trimmed string containing the user input
     */
    private static String readUserInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().trim();
    }

    /**
     * Validates the user input to ensure it contains exactly ten integers.
     *
     * @param userInput the input string from the user
     * @return an array of strings, each representing a valid integer
     * @throws IllegalStateException    if the input does not contain exactly ten integers
     * @throws IllegalArgumentException if any of the inputs are not valid integers
     */
    private static String[] validateUserInput(String userInput) {
        String[] numbers = userInput.split("\\s+");
        if (numbers.length != 10) {
            throw new IllegalStateException();
        }
        for (String number : numbers) {
            if (!number.matches("\\d+")) {
                throw new IllegalArgumentException();
            }
        }
        return numbers;
    }

    /**
     * Converts an array of string representations of integers to an array of integers.
     *
     * @param numbersStr the array of strings to convert
     * @return an array of integers
     */
    private static int[] convertToInteger(String[] numbersStr) {
        int[] numbers = new int[10];
        for (int i = 0; i < numbersStr.length; i++) {
            numbers[i] = Integer.parseInt(numbersStr[i]);
        }
        return numbers;
    }
}
