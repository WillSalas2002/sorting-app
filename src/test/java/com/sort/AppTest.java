package com.sort;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

@RunWith(Parameterized.class)
public class AppTest {
    private final String input;
    private final String expectedOutput;
    private final Class<? extends Exception> expectedException;

    public AppTest(String input, String expectedOutput, Class<? extends Exception> expectedException) {
        this.input = input;
        this.expectedOutput = expectedOutput;
        this.expectedException = expectedException;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"", null, NoSuchElementException.class},
                {"5", null, IllegalStateException.class},
                {"10 9 8 7 6 5 4 3 2 1", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", null},
                {"1 2 3 4 5 6 7 8 9 10 11", null, IllegalStateException.class},
                {"1 2 3 4 5 6 7 8 9 a", null, IllegalArgumentException.class}
        });
    }

    @Test
    public void testSortingApp() {
        InputStream originalIn = System.in;
        PrintStream originalOut = System.out;
        try {
            System.setIn(new ByteArrayInputStream(input.getBytes()));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            System.setOut(new PrintStream(out));

            if (expectedException != null) {
                assertThrows(expectedException, App::main);
            } else {
                App.main();
                String output = out.toString().trim();
                String[] outputLines = output.split(System.lineSeparator());
                String sortedNumbers = outputLines[outputLines.length - 1].trim();
                assertEquals(expectedOutput, sortedNumbers);
            }
        } finally {
            System.setIn(originalIn);
            System.setOut(originalOut);
        }
    }
}

