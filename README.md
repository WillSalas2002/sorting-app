# Sorting App

This is a simple Java application built with Maven that sorts up to ten integers provided via command-line input.

## Features

- Reads up to ten integers from the user.
- Validates input to ensure it contains exactly ten integers.
- Sorts the integers in ascending order.
- Prints the sorted integers to the console.

## Prerequisites

- Java Development Kit (JDK) 8 or higher installed.
- Apache Maven installed and configured.

## Getting Started

### Clone the Repository

Clone the repository to your local machine using Git:

```bash
git clone https://github.com/your-username/sorting-app.git
cd sorting-app
```

### Build the Project

Build the Maven project using the following command:

```bash
mvn clean package
```

### Run the Application

To run the application, use the following command:

```bash
java -jar SortingApp-1.0-SNAPSHOT.jar

```
